# JdR



## Getting started

https://mudblazor.com/getting-started/installation#64fca65e-9c40-437b-83e4-26925ebe0f70

SDK .NET 8.0
MudBlazor

## mudblazor dotnet templates

```
dotnet new install MudBlazor.Templates
```

## create new project mudblazor

inside folder

```
dotnet new mudblazor --host wasm --name Kotatsu
```

## dotnet watch

tool to refresh automatically localhost

```
dotnet tool install -g dotnet-watch
```

add Watch reference in .csproj
```
<Project Sdk="Microsoft.NET.Sdk.Web">
    <PropertyGroup>
        <OutputType>Exe</OutputType>
        <TargetFramework>net5.0</TargetFramework>
    </PropertyGroup>

    <ItemGroup>
        <PackageReference Include="MudBlazor" Version="5.0.2" />
    </ItemGroup>

    <ItemGroup>
        <DotNetCliToolReference Include="Microsoft.DotNet.Watcher.Tools" Version="2.0.0" />
    </ItemGroup>
</Project>
```

command to use :
```
dotnet watch run
```